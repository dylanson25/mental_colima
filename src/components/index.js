import CustomButton from './CustomButtons/customButton';
import  ProfileBtn from './CustomButtons/ProfileButtons';
import CustomInput from './CustomInput/inputComponent';
import InputCedula from './InputCedula/inputCedula';
import { RadioButton } from './RadioButton/RadioButons';
import CardPath from './CardPath/CardPath';
import pathData from './CardPath/pathData';
import PerfilInfo from './PerfilInfo/PerfilInfo';
import MessageToVerify from './VerifyUser/MessageToVerify';
export {
    CustomInput,
    CustomButton,
    InputCedula,
    RadioButton,
    MessageToVerify,
    CardPath,
    pathData,
    ProfileBtn,
    PerfilInfo
}