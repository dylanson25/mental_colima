import { firbaseMethods } from "./firbase.method";
import { getNewState } from "./utils";

export {
    firbaseMethods,
    getNewState
}