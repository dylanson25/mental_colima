import Login from './login.screen';
import Path from './path.screen';
import Register from './register.screen';
import { Profile, EditProfile, ChangePswrd } from './Perfil';
import  Verify  from './verifyUser.screen';
import CardProList from './cardProList.screen';
import EditProCard from './editProCard.screen';
export {
    Login,
    Path,
    Register,
    Verify,
    Profile,
    CardProList,
    EditProCard,
    EditProfile,
    ChangePswrd
}